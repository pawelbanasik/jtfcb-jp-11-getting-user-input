import java.util.Scanner;

public class App {

	public static void main(String[] args) {

		// create scanner object
		Scanner input = new Scanner(System.in);

		// output the prompt (mozna integer, double itd.)
		System.out.println("Enter a line of text: ");

		// wait for the user to enter a line of text
		String line = input.nextLine();
		// dla inta
		// int myInt = input.nextInt();
		// dla double
		// double myDouble = input.nextDouble();


		// tell them what they entered
		System.out.println("You entered: " + line);

	}

}
